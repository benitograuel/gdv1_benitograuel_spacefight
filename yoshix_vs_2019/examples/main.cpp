
#include "yoshix_fix_function.h"

#include <math.h>
#include <random>
#include <iostream>

using namespace gfx;

//global
//======== Gameboard const ========//
int constexpr       Gameboard_Width = 30;
int constexpr       Gameboard_Height = 20;
float constexpr     BORDER_LENGTH = 1.0f;
float constexpr     COLISION_DISTANCE = BORDER_LENGTH * 0.5f;

//======== Spaceship const ========//
float constexpr     SSHIP_LENGTH = 2.8f;

float constexpr     SSHIP_ONE_Y1 = 5;
float constexpr     SSHIP_ONE_X1 = Gameboard_Width / 2;
float constexpr     SSHIP_VELOCITY = 0.3f;

///////////Bullet const///////////
float constexpr     BULLET_SIZE = 0.2f;
float constexpr     BULLET_VELOCITY_Y = 0.3f;
float constexpr     BULLET_START_POSITION_X = SSHIP_ONE_X1;
float constexpr     BULLET_START_POSITION_Y = SSHIP_ONE_Y1;

///////////Enemy1 const///////////
float constexpr     ENEMY1_LENGTH = 2.5f;
float constexpr     ENEMY1_ONE_Y1 = 22;
float               ENEMY1_ONE_X1 = 15;
float constexpr     ENEMY1_VELOCITY = 0.05f;

///////////Enemy2 const///////////
float constexpr     ENEMY2_LENGTH = 2.5f;
float constexpr     ENEMY2_TWO_Y2 = 20;
float               ENEMY2_TWO_X2 = 7;
float constexpr     ENEMY2_VELOCITY = 0.05f;

///////////Meteor const///////////
float constexpr     METEOR_LENGTH = 1.0f;
float constexpr     METEOR_Y = 3;
float               METEOR_X = 18;
float constexpr     METEOR_VELOCITY = 0.05f;
float constexpr     METEOR_VELOCITX = 0.006f;

int                 Score = 0;
int                 MeteorHitCounter = 0;
bool                SpawnSecondBullet = false;
bool                hasStarted = false;

namespace
{
    class CApplication : public IApplication
    {
        public:

            CApplication();
            virtual ~CApplication();

    private:

        // Meshes

        BHandle SpaceshipMesh;
        BHandle SpaceshipTexture;

        BHandle BulletMesh;
        BHandle BulletTexture;

        BHandle BackgroundMesh;
        BHandle BackgroundTexture;

        BHandle EnemyTexture;
        BHandle EnemyMesh;

        BHandle MeteorTexture;
        BHandle MeteorMesh;


        float			Winkel_Y;

        // Plattform:
        // x & y for position of the platform
        // acceleration in y direction
        float           SpaceshipPosition_X1;
        float           SpaceshipPosition_Y1;
        float           SpaceshipMoveX;

        // Enemy 1
        float           EnemyPosition_X1;
        float           EnemyPosition_Y1;
        float           EnemyMoveY;

        // Enemy 2
        float           EnemyPosition_X2;
        float           EnemyPosition_Y2;

        // Meteor
        float           MeteorPosition_X;
        float           MeteorPosition_Y;
        float           MeteorMoveY;
        float           MeteorMoveX;

        // Bullet
        // x & y for position of the ball
        // velocity for direction of ball in x & y direction
        float           BulletPosition_X;
        float	        BulletPosition_Y;
        float           BulletMoveY;

        // Bullet 2
        // x & y for position of the ball
        // velocity for direction of ball in x & y direction
        float           BulletPosition_X2;
        float	        BulletPosition_Y2;
        float           BulletMoveY2;


//====================================================================================


        private:

            void Cube(BHandle* _ppMesh, BHandle _pTexture, float _EdgeLength);
            void Sphere(BHandle* _ppMesh, BHandle _pTexture, float _Radius);
            bool CollisionTest();
            bool CollisionTestMeteor();

        private:

            virtual bool InternOnStartup();
            virtual bool InternOnShutdown();
            virtual bool InternOnCreateTextures();
            virtual bool InternOnReleaseTextures();
            virtual bool InternOnCreateMeshes();
            virtual bool InternOnReleaseMeshes();
            virtual bool InternOnResize(int _Width, int _Height);
            virtual bool InternOnKeyEvent(unsigned int _Key, bool _IsKeyDown, bool _IsAltDown);
            virtual bool InternOnUpdate();
            virtual bool InternOnFrame();
    };
}

namespace
{
    CApplication::CApplication()
        : Winkel_Y(60.0f)
            , EnemyPosition_X1(ENEMY1_ONE_X1)
            , EnemyPosition_Y1(ENEMY1_ONE_Y1)
            , EnemyMesh(nullptr)
            , EnemyTexture(nullptr)
            , MeteorPosition_X(METEOR_X)
            , MeteorPosition_Y(METEOR_Y)
            , MeteorMoveY(METEOR_VELOCITY)
            , MeteorMoveX(METEOR_VELOCITX)
            , MeteorTexture(nullptr)
            , MeteorMesh(nullptr)
            , EnemyPosition_X2(ENEMY2_TWO_X2)
            , EnemyPosition_Y2(ENEMY2_TWO_Y2)
            , EnemyMoveY(0)
            , BulletMesh(nullptr)
            , BulletTexture(nullptr)
            , BulletMoveY(0)
            , BulletPosition_X(BULLET_START_POSITION_X)
            , BulletPosition_Y(BULLET_START_POSITION_Y)
            , BulletMoveY2(0)
            , BulletPosition_X2(BULLET_START_POSITION_X)
            , BulletPosition_Y2(BULLET_START_POSITION_Y)
            , SpaceshipMesh(nullptr)
            , SpaceshipTexture(nullptr)
            , BackgroundMesh(nullptr)
            , BackgroundTexture(nullptr)
            , SpaceshipPosition_X1(SSHIP_ONE_X1)
            , SpaceshipPosition_Y1(SSHIP_ONE_Y1)
            , SpaceshipMoveX(SSHIP_VELOCITY)
    {
    }

    // -----------------------------------------------------------------------------

    CApplication::~CApplication()
    {
    }

    // -----------------------------------------------------------------------------

    bool CApplication::InternOnStartup()
    {
        //ToDo Light
        float LightPosition[3] = { Gameboard_Width / 2, Gameboard_Height / 2, -15.0f, };

        SetLightPosition(LightPosition);

        // Ambientes Licht, Diffuses Licht, Spekulares Licht

        float LightAmbientColor[4] = { 0.3f, 0.3f, 0.3f, 1.0f, };
        float LightDiffuseColor[4] = { 1.0f, 1.0f, 1.0f, 1.0f, };
        float LightSpecularColor[4] = { 1.0f, 1.0f, 1.0f, 1.0f, };

        SetLightColor(LightAmbientColor, LightDiffuseColor, LightSpecularColor, 127);

        return true;
    }

    // -----------------------------------------------------------------------------

    bool CApplication::InternOnShutdown()
    {
        return true;
    }


    bool CApplication::InternOnCreateTextures()
    {
        //Texture Data
        CreateTexture("..\\data\\images\\bullets.dds", &BulletTexture);
        CreateTexture("..\\data\\images\\space.dds", &BackgroundTexture);
        CreateTexture("..\\data\\images\\spaceship.dds", &SpaceshipTexture);
        CreateTexture("..\\data\\images\\enemy.dds", &EnemyTexture);
        CreateTexture("..\\data\\images\\meteor.dds", &MeteorTexture);
        return true;
    }

    // -----------------------------------------------------------------------------

    bool CApplication::InternOnReleaseTextures()
    {
        ReleaseTexture(BulletTexture);
        ReleaseTexture(BackgroundTexture);
        ReleaseTexture(SpaceshipTexture);
        ReleaseTexture(EnemyTexture);
        ReleaseTexture(MeteorTexture);
        return true;
    }

    // -----------------------------------------------------------------------------

    bool CApplication::InternOnCreateMeshes()
    {
        Sphere(&BulletMesh, BulletTexture, 1.0f);
        Cube(&BackgroundMesh, BackgroundTexture, 1.0f);
        Cube(&SpaceshipMesh, SpaceshipTexture, 1.0f);
        Cube(&EnemyMesh, EnemyTexture, 1.0f);
        Cube(&MeteorMesh, MeteorTexture, 1.0f);
        return true;
    }

    bool CApplication::InternOnReleaseMeshes()
    {
        ReleaseMesh(BulletMesh);
        ReleaseMesh(BackgroundMesh);
        ReleaseMesh(SpaceshipMesh);
        ReleaseMesh(EnemyMesh);
        ReleaseMesh(MeteorMesh);
        return true;
    }

    bool CApplication::InternOnResize(int _Width, int _Height)
    {
        float ProjectionMatrix[16];

        GetProjectionMatrix(Winkel_Y, static_cast<float>(_Width) / static_cast<float>(_Height), 0.1f, 1000.0f, ProjectionMatrix);

        SetProjectionMatrix(ProjectionMatrix);
        return true;
    }


    bool CApplication::InternOnUpdate()
    {
        float Eye[3];
        float At[3];
        float Up[3];

        float ViewMatrix[16];

        float height = Gameboard_Height / 2;
        float width = Gameboard_Width / 2;


        Eye[0] = width;        At[0] = width;      Up[0] = 0.0f;
        Eye[1] = height;       At[1] = height;     Up[1] = 1.0f;
        Eye[2] = -13.0f;        At[2] = 0.0f;       Up[2] = 0.0f;

        GetViewMatrix(Eye, At, Up, ViewMatrix);

        SetViewMatrix(ViewMatrix);
        return true;
    }


    bool CApplication::InternOnFrame()
    {
        float WorldMatrix[16];
        float ScaleMatrix[16];
        float TranslationMatrix[16];

        if(BulletPosition_Y > 20)
        {
            BulletPosition_Y = SpaceshipPosition_Y1;
            BulletPosition_X = SpaceshipPosition_X1;
            BulletMoveY = 0;
        }

        if(BulletPosition_Y == SSHIP_ONE_Y1)
        {
            BulletPosition_X = SpaceshipPosition_X1;
        }


        if (BulletPosition_Y2 > 20)
        {
            BulletPosition_Y2 = SpaceshipPosition_Y1;
            BulletPosition_X2 = SpaceshipPosition_X1;
            BulletMoveY2 = 0;
        }

        if (BulletPosition_Y2 == SSHIP_ONE_Y1)
        {
            BulletPosition_X2 = SpaceshipPosition_X1;
        }

        // Draw Background
        GetTranslationMatrix(Gameboard_Width / 2, Gameboard_Height / 2, 1.0f, TranslationMatrix);
        GetScaleMatrix(30.0f, 30.0f, 0.0f, ScaleMatrix);
        MulMatrix(ScaleMatrix, TranslationMatrix, WorldMatrix);
        SetWorldMatrix(WorldMatrix);
        DrawMesh(BackgroundMesh);

        //-----------Spaceship----------- //
        GetTranslationMatrix(SpaceshipPosition_X1, SpaceshipPosition_Y1, 0.0f, TranslationMatrix);
        GetScaleMatrix(SSHIP_LENGTH, 0.2f, 0.1f, ScaleMatrix);
        MulMatrix(ScaleMatrix, TranslationMatrix, WorldMatrix);
        SetWorldMatrix(WorldMatrix);
        DrawMesh(SpaceshipMesh);

        ////Bullet////
        GetScaleMatrix(BULLET_SIZE, BULLET_SIZE, BULLET_SIZE, ScaleMatrix);
        GetTranslationMatrix(BulletPosition_X, BulletPosition_Y, 0.0f, TranslationMatrix);
        MulMatrix(ScaleMatrix, TranslationMatrix, WorldMatrix);

        SetWorldMatrix(WorldMatrix);
        DrawMesh(BulletMesh);

        BulletPosition_Y += BulletMoveY;

        // Hit Meteor twice -> Spawn Second Bullet
        if (SpawnSecondBullet == true) {
            ////Bullet////
            GetScaleMatrix(BULLET_SIZE+0.2, BULLET_SIZE+0.2, BULLET_SIZE+0.2, ScaleMatrix);
            GetTranslationMatrix(BulletPosition_X2, BulletPosition_Y2, 0.0f, TranslationMatrix);
            MulMatrix(ScaleMatrix, TranslationMatrix, WorldMatrix);

            SetWorldMatrix(WorldMatrix);
            DrawMesh(BulletMesh);

            BulletPosition_Y2 += BulletMoveY2;
        }

        //-----------Enemy----------- //

        GetTranslationMatrix(EnemyPosition_X1, EnemyPosition_Y1, 0.0f, TranslationMatrix);
        GetScaleMatrix(ENEMY1_LENGTH, 0.1f, 0.6f, ScaleMatrix);
        MulMatrix(ScaleMatrix, TranslationMatrix, WorldMatrix);
        SetWorldMatrix(WorldMatrix);
        DrawMesh(EnemyMesh);

        EnemyPosition_Y1 -= EnemyMoveY;
        EnemyMoveY = ENEMY1_VELOCITY;

        //-----------Meteor----------- //
        GetTranslationMatrix(MeteorPosition_X, MeteorPosition_Y, 0.0f, TranslationMatrix);
        GetScaleMatrix(METEOR_LENGTH, METEOR_LENGTH, METEOR_LENGTH, ScaleMatrix);
        MulMatrix(ScaleMatrix, TranslationMatrix, WorldMatrix);
        SetWorldMatrix(WorldMatrix);
        DrawMesh(MeteorMesh);

        MeteorPosition_Y -= MeteorMoveY;
        MeteorPosition_X -= MeteorMoveX;

        MeteorMoveY = METEOR_VELOCITY;
        MeteorMoveX = METEOR_VELOCITX;
        

        // when score is over 10 -> spawn another enemy
        if (Score > 10) {
            //-----------Enemy----------- //

            GetTranslationMatrix(EnemyPosition_X2, EnemyPosition_Y2, 0.0f, TranslationMatrix);
            GetScaleMatrix(ENEMY2_LENGTH, 0.1f, 0.1f, ScaleMatrix);
            MulMatrix(ScaleMatrix, TranslationMatrix, WorldMatrix);
            SetWorldMatrix(WorldMatrix);
            DrawMesh(EnemyMesh);

            EnemyPosition_Y2 -= EnemyMoveY;
            EnemyMoveY = ENEMY2_VELOCITY;
        }

        // when hit then reset enemy position & bullet
        // Hit Enemy1
        if (abs(BulletPosition_Y - EnemyPosition_Y1) < 0.5 && abs(BulletPosition_X - EnemyPosition_X1) < 2)
        {
            EnemyPosition_Y1 = 20;
            EnemyPosition_X1 = rand() % 20 + 5;
            BulletPosition_Y = SpaceshipPosition_Y1;
            BulletPosition_X = SpaceshipPosition_X1;
            BulletMoveY = 0;

            Score = Score + 1;
        }

        // Hit Enemy2
        if (abs(BulletPosition_Y - EnemyPosition_Y2) < 0.5 && abs(BulletPosition_X - EnemyPosition_X2) < 2)
        {
            EnemyPosition_Y2 = 20;
            EnemyPosition_X2 = rand() % 20 + 5;
            BulletPosition_Y = SpaceshipPosition_Y1;
            BulletPosition_X = SpaceshipPosition_X1;
            BulletMoveY = 0;

            Score = Score + 1;
        }

        // when Bullet 2 hit then reset enemy position & bullet
        // Hit Enemy1
        if (abs(BulletPosition_Y2 - EnemyPosition_Y1) < 0.5 && abs(BulletPosition_X2 - EnemyPosition_X1) < 2)
        {
            EnemyPosition_Y1 = 20;
            EnemyPosition_X1 = rand() % 20 + 5;
            BulletPosition_Y2 = SpaceshipPosition_Y1;
            BulletPosition_X2 = SpaceshipPosition_X1;
            BulletMoveY2 = 0;

            Score = Score + 1;
        }

        // Hit Enemy2
        if (abs(BulletPosition_Y2 - EnemyPosition_Y2) < 0.5 && abs(BulletPosition_X2 - EnemyPosition_X2) < 2)
        {
            EnemyPosition_Y2 = 20;
            EnemyPosition_X2 = rand() % 20 + 5;
            BulletPosition_Y2 = SpaceshipPosition_Y1;
            BulletPosition_X2 = SpaceshipPosition_X1;
            BulletMoveY2 = 0;

            Score = Score + 1;
        }

        // Hit Meteor
        if (abs(BulletPosition_Y - MeteorPosition_Y) < 0.5 && abs(BulletPosition_X - MeteorPosition_X) < 0.5)
        {
            MeteorHitCounter += 1;

            if (MeteorHitCounter >= 2) {
                SpawnSecondBullet = true;
            }

            BulletPosition_Y = SpaceshipPosition_Y1;
            BulletPosition_X = SpaceshipPosition_X1;
            BulletMoveY = 0;
        }

        // Hit Meteor with Bigger Bullet
        if (abs(BulletPosition_Y2 - MeteorPosition_Y) < 0.5 && abs(BulletPosition_X2 - MeteorPosition_X) < 0.5)
        {
            MeteorPosition_Y = 30;
        }

        // Enemy came through -> Game Over
        if (EnemyPosition_Y1 < SSHIP_ONE_Y1+0.8) 
        {
            EnemyPosition_X1 = rand() % 20 + 5;
            StopApplication();
        }

        if (EnemyPosition_Y2 < SSHIP_ONE_Y1+0.8)
        {
            EnemyPosition_X2 = rand() % 20 + 5;
            StopApplication();
        }

        // if a meteor hits the spaceship -> game over
        if (abs(SpaceshipPosition_Y1 - MeteorPosition_Y) < 0.5 && abs(SpaceshipPosition_X1 - MeteorPosition_X) < 1.5)
        {
            StopApplication();
        }

        // Meteor came through
        if (MeteorPosition_Y < SSHIP_ONE_Y1-2)
        {
            MeteorPosition_X = rand() % 20 + 5;
            MeteorPosition_Y = 20;
        }
        return true;
    }

    bool CApplication::InternOnKeyEvent(unsigned int _Key, bool _IsKeyDown, bool _IsAltDown)
    {
        if (_Key == 'N')
        {
            if(SpaceshipPosition_X1 < Gameboard_Width - SSHIP_LENGTH)
            {
                SpaceshipPosition_X1 += SpaceshipMoveX;
            }

        }
        if (_Key == 'C')
        {
            if(SpaceshipPosition_X1 > BORDER_LENGTH * 2)
            {
                SpaceshipPosition_X1 -= SpaceshipMoveX;
            }
        }

        if (_Key == ' ') 
        {
            BulletMoveY = BULLET_VELOCITY_Y;
        }

        if (_Key == 'B')
        {
            BulletMoveY2 = BULLET_VELOCITY_Y- 0.15;
        }

        return true;
    }

    bool CApplication::CollisionTest()
    {
        if (BulletPosition_Y == EnemyPosition_Y1)
        {
            return true;
        }
        else {
            return false;
        }
    }

    bool CApplication::CollisionTestMeteor()
    {
        if (BulletPosition_Y == MeteorPosition_Y)
        {
            return true;
        }
        else {
            return false;
        }
    }

    void CApplication::Sphere(BHandle* _ppMesh, BHandle _pTexture, float _Radius)
    {
        static const float s_Pi = 3.1415926535897932384626433832795f;
        static const int   s_Delta = 10;
        static const int   s_NumberOfVerticalVertices = 180 / s_Delta + 1;
        static const int   s_NumberOfHorizontalVertices = 360 / s_Delta + 1;
        static const int   s_NumberOfVertices = s_NumberOfVerticalVertices * s_NumberOfHorizontalVertices;
        static const int   s_NumberOfIndices = s_NumberOfVertices * 2 * 3;

        int   IndexOfVertex;
        int   IndexOfIndex;

        float RadiusOfSphere;
        float CenterOfSphere[3];
        float RadiusOfHorizontalCircle;
        float CenterOfHorizontalCircle[3];
        float FirstVertexOfHorizontalCircle[3];
        float Distance[3];

        int   Indices[s_NumberOfIndices];
        float Vertices[s_NumberOfVertices * 3];
        float TexCoords[s_NumberOfVertices * 2];

        RadiusOfSphere = _Radius;

        CenterOfSphere[0] = 0.0f;
        CenterOfSphere[1] = 0.0f;
        CenterOfSphere[2] = 0.0f;

        IndexOfVertex = 0;

        for (float Alpha = 90.0f; Alpha <= 270; Alpha += s_Delta)
        {
            FirstVertexOfHorizontalCircle[0] = CenterOfSphere[0] + RadiusOfSphere * cos(s_Pi * Alpha / 180.0f);
            FirstVertexOfHorizontalCircle[1] = CenterOfSphere[1] + RadiusOfSphere * sin(s_Pi * Alpha / 180.0f);
            FirstVertexOfHorizontalCircle[2] = CenterOfSphere[2];

            CenterOfHorizontalCircle[0] = CenterOfSphere[0];
            CenterOfHorizontalCircle[1] = FirstVertexOfHorizontalCircle[1];
            CenterOfHorizontalCircle[2] = CenterOfSphere[2];

            Distance[0] = FirstVertexOfHorizontalCircle[0] - CenterOfHorizontalCircle[0];
            Distance[1] = FirstVertexOfHorizontalCircle[1] - CenterOfHorizontalCircle[1];
            Distance[2] = FirstVertexOfHorizontalCircle[2] - CenterOfHorizontalCircle[2];

            RadiusOfHorizontalCircle = sqrtf(Distance[0] * Distance[0] + Distance[1] * Distance[1] + Distance[2] * Distance[2]);

            for (float Gamma = 0.0f; Gamma <= 360; Gamma += s_Delta)
            {
                Vertices[IndexOfVertex * 3 + 0] = CenterOfHorizontalCircle[0] + RadiusOfHorizontalCircle * cos(s_Pi * Gamma / 180.0f);
                Vertices[IndexOfVertex * 3 + 1] = CenterOfHorizontalCircle[1];
                Vertices[IndexOfVertex * 3 + 2] = CenterOfHorizontalCircle[2] + RadiusOfHorizontalCircle * sin(s_Pi * Gamma / 180.0f);

                TexCoords[IndexOfVertex * 2 + 0] = Gamma / 360.0f;
                TexCoords[IndexOfVertex * 2 + 1] = (Alpha - 90.0f) / 180.0f;

                ++IndexOfVertex;
            }
        }

        IndexOfIndex = 0;

        for (int IndexOfCircle = 0; IndexOfCircle < s_NumberOfVerticalVertices; ++IndexOfCircle)
        {
            int FirstIndexOfCircle = IndexOfCircle * s_NumberOfHorizontalVertices;

            for (int IndexOfTriangle = 0; IndexOfTriangle < s_NumberOfHorizontalVertices; ++IndexOfTriangle)
            {
                int UpperLeft = FirstIndexOfCircle + 0 + IndexOfTriangle;
                int UpperRight = FirstIndexOfCircle + 0 + IndexOfTriangle + 1;
                int LowerLeft = FirstIndexOfCircle + s_NumberOfHorizontalVertices + IndexOfTriangle;
                int LowerRight = FirstIndexOfCircle + s_NumberOfHorizontalVertices + IndexOfTriangle + 1;

                Indices[IndexOfIndex + 0] = LowerLeft;
                Indices[IndexOfIndex + 1] = LowerRight;
                Indices[IndexOfIndex + 2] = UpperRight;

                Indices[IndexOfIndex + 3] = LowerLeft;
                Indices[IndexOfIndex + 4] = UpperRight;
                Indices[IndexOfIndex + 5] = UpperLeft;

                IndexOfIndex += 6;
            }
        }

        // -----------------------------------------------------------------------------
        // Define the mesh and its material. The material defines the look of the
        // surface covering the mesh. If the material should contain normals, colors, or
        // texture coordinates then their number has to match the number of vertices.
        // If you do not support normals in a mesh, YoshiX will not apply lighting to
        // this mesh.
        // -----------------------------------------------------------------------------
        SMeshInfo MeshInfo;

        MeshInfo.m_pVertices = Vertices;
        MeshInfo.m_pNormals = nullptr;
        MeshInfo.m_pColors = nullptr;                      // No colors.
        MeshInfo.m_pTexCoords = TexCoords;
        MeshInfo.m_NumberOfVertices = s_NumberOfVertices;
        MeshInfo.m_NumberOfIndices = s_NumberOfIndices;
        MeshInfo.m_pIndices = Indices;
        MeshInfo.m_pTexture = _pTexture;

        CreateMesh(MeshInfo, _ppMesh);
    }

    void CApplication::Cube(BHandle* _ppMesh, BHandle _pTexture, float _EdgeLength)
    {
        // -----------------------------------------------------------------------------
        // Define the vertices of the mesh and their attributes.
        // -----------------------------------------------------------------------------
        float s_HalfEdgeLength = 0.5f * _EdgeLength;

        float s_CubeVertices[][3] =
        {
            { -s_HalfEdgeLength, -s_HalfEdgeLength, -s_HalfEdgeLength, },
            {  s_HalfEdgeLength, -s_HalfEdgeLength, -s_HalfEdgeLength, },
            {  s_HalfEdgeLength,  s_HalfEdgeLength, -s_HalfEdgeLength, },
            { -s_HalfEdgeLength,  s_HalfEdgeLength, -s_HalfEdgeLength, },

            {  s_HalfEdgeLength, -s_HalfEdgeLength, -s_HalfEdgeLength, },
            {  s_HalfEdgeLength, -s_HalfEdgeLength,  s_HalfEdgeLength, },
            {  s_HalfEdgeLength,  s_HalfEdgeLength,  s_HalfEdgeLength, },
            {  s_HalfEdgeLength,  s_HalfEdgeLength, -s_HalfEdgeLength, },

            {  s_HalfEdgeLength, -s_HalfEdgeLength,  s_HalfEdgeLength, },
            { -s_HalfEdgeLength, -s_HalfEdgeLength,  s_HalfEdgeLength, },
            { -s_HalfEdgeLength,  s_HalfEdgeLength,  s_HalfEdgeLength, },
            {  s_HalfEdgeLength,  s_HalfEdgeLength,  s_HalfEdgeLength, },

            { -s_HalfEdgeLength, -s_HalfEdgeLength,  s_HalfEdgeLength, },
            { -s_HalfEdgeLength, -s_HalfEdgeLength, -s_HalfEdgeLength, },
            { -s_HalfEdgeLength,  s_HalfEdgeLength, -s_HalfEdgeLength, },
            { -s_HalfEdgeLength,  s_HalfEdgeLength,  s_HalfEdgeLength, },

            { -s_HalfEdgeLength,  s_HalfEdgeLength, -s_HalfEdgeLength, },
            {  s_HalfEdgeLength,  s_HalfEdgeLength, -s_HalfEdgeLength, },
            {  s_HalfEdgeLength,  s_HalfEdgeLength,  s_HalfEdgeLength, },
            { -s_HalfEdgeLength,  s_HalfEdgeLength,  s_HalfEdgeLength, },

            { -s_HalfEdgeLength, -s_HalfEdgeLength,  s_HalfEdgeLength, },
            {  s_HalfEdgeLength, -s_HalfEdgeLength,  s_HalfEdgeLength, },
            {  s_HalfEdgeLength, -s_HalfEdgeLength, -s_HalfEdgeLength, },
            { -s_HalfEdgeLength, -s_HalfEdgeLength, -s_HalfEdgeLength, },
        };

        static float s_CubeNormals[][3] =
        {
            {  0.0f,  0.0f, -1.0f, },
            {  0.0f,  0.0f, -1.0f, },
            {  0.0f,  0.0f, -1.0f, },
            {  0.0f,  0.0f, -1.0f, },

            {  1.0f,  0.0f,  0.0f, },
            {  1.0f,  0.0f,  0.0f, },
            {  1.0f,  0.0f,  0.0f, },
            {  1.0f,  0.0f,  0.0f, },

            {  0.0f,  0.0f,  1.0f, },
            {  0.0f,  0.0f,  1.0f, },
            {  0.0f,  0.0f,  1.0f, },
            {  0.0f,  0.0f,  1.0f, },

            { -1.0f,  0.0f,  0.0f, },
            { -1.0f,  0.0f,  0.0f, },
            { -1.0f,  0.0f,  0.0f, },
            { -1.0f,  0.0f,  0.0f, },

            {  0.0f,  1.0f,  0.0f, },
            {  0.0f,  1.0f,  0.0f, },
            {  0.0f,  1.0f,  0.0f, },
            {  0.0f,  1.0f,  0.0f, },

            {  0.0f, -1.0f,  0.0f, },
            {  0.0f, -1.0f,  0.0f, },
            {  0.0f, -1.0f,  0.0f, },
            {  0.0f, -1.0f,  0.0f, },
        };

        float s_U[] =
        {
            0.0f / 4.0f,
            1.0f / 4.0f,
            2.0f / 4.0f,
            3.0f / 4.0f,
            4.0f / 4.0f,
        };

        float s_V[] =
        {
            3.0f / 3.0f,
            2.0f / 3.0f,
            1.0f / 3.0f,
            0.0f / 3.0f,
        };

        float s_CubeTexCoords[][2] =
        {
            { s_U[1], s_V[1], },
            { s_U[2], s_V[1], },
            { s_U[2], s_V[2], },
            { s_U[1], s_V[2], },

            { s_U[2], s_V[1], },
            { s_U[3], s_V[1], },
            { s_U[3], s_V[2], },
            { s_U[2], s_V[2], },

            { s_U[3], s_V[1], },
            { s_U[4], s_V[1], },
            { s_U[4], s_V[2], },
            { s_U[3], s_V[2], },

            { s_U[0], s_V[1], },
            { s_U[1], s_V[1], },
            { s_U[1], s_V[2], },
            { s_U[0], s_V[2], },

            { s_U[1], s_V[2], },
            { s_U[2], s_V[2], },
            { s_U[2], s_V[3], },
            { s_U[1], s_V[3], },

            { s_U[1], s_V[0], },
            { s_U[2], s_V[0], },
            { s_U[2], s_V[1], },
            { s_U[1], s_V[1], },
        };

        // -----------------------------------------------------------------------------
        // Define the topology of the mesh via indices. An index addresses a vertex from
        // the array above. Three indices represent one triangle. When defining the 
        // triangles of a mesh imagine that you are standing in front of the triangle 
        // and looking to the center of the triangle. If the mesh represents a closed
        // body such as a cube, your view position has to be outside of the body. Now
        // define the indices of the addressed vertices of the triangle in counter-
        // clockwise order.
        // -----------------------------------------------------------------------------
        int s_CubeIndices[][3] =
        {
            {  0,  1,  2, },
            {  0,  2,  3, },

            {  4,  5,  6, },
            {  4,  6,  7, },

            {  8,  9, 10, },
            {  8, 10, 11, },

            { 12, 13, 14, },
            { 12, 14, 15, },

            { 16, 17, 18, },
            { 16, 18, 19, },

            { 20, 21, 22, },
            { 20, 22, 23, },
        };

        // -----------------------------------------------------------------------------
        // Define the mesh and its material. The material defines the look of the
        // surface covering the mesh. If the material should contain normals, colors, or
        // texture coordinates then their number has to match the number of vertices.
        // If you do not support normals in a mesh, YoshiX will not apply lighting to
        // this mesh. A textured mesh always has to contain texture coordinates and a
        // handle to a texture. A mesh always has to contain vertices and indices.
        // -----------------------------------------------------------------------------
        SMeshInfo MeshInfo;

        MeshInfo.m_pVertices = &s_CubeVertices[0][0];
        MeshInfo.m_pNormals = &s_CubeNormals[0][0];
        MeshInfo.m_pColors = nullptr;                          // No colors
        MeshInfo.m_pTexCoords = &s_CubeTexCoords[0][0];
        MeshInfo.m_NumberOfVertices = 24;
        MeshInfo.m_NumberOfIndices = 36;
        MeshInfo.m_pIndices = &s_CubeIndices[0][0];
        MeshInfo.m_pTexture = _pTexture;


        CreateMesh(MeshInfo, _ppMesh);
    }
}

void main()
{
    puts("This is a Space-Shooter Game");
    puts("Move left press:      C");
    puts("Move Right press:     N");
    puts("Shoot Bullet:         Space");
    puts("::::::::::::::::::::::::::::::::::::::::::::::::::::");
    puts("To get a second bigget Bullet, shoot a Meteor twice!");
    puts("Shoot Bigger Bullet:  B");
    puts("::::::::::::::::::::::::::::::::::::::::::::::::::::");
    puts("If a enemy passes by or a meteor hits you, Game Over!");

    system("pause");
    CApplication Application;
    

    RunApplication(1080, 720, "Game Benito", &Application);
    puts("::::::::::::::::::::::::::::::::");
    puts("::::::::::::GAME OVER:::::::::::");
    puts("::::::::::::::::::::::::::::::::");
    puts("________________________________");
    puts("____________Your Score__________");
    std::cout << Score;
}
